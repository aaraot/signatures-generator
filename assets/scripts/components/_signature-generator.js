class signatureGenerator {
    constructor() {
        this.form = {
            // pic: '',
            name: '',
            role: '',
            contact: '',
            email: ''
        };

        $('select').formSelect();
    }

    generate() {
        this.fetchData();

        for (var key in this.form) {
            const elem = document.querySelectorAll('.signature-' + key);

            elem.forEach(element => {
                if (element.hasAttribute('href')) {
                    const href = element.getAttribute('href');

                    if (href.includes('tel')) {
                        element.setAttribute('href', 'tel:' + this.form[key]);
                    } else if (href.includes('mailto')) {
                        element.setAttribute('href', 'mailto:' + this.form[key]);
                    } else {
                        element.setAttribute('href', this.form[key]);
                    }
                } else {
                    element.innerText = this.form[key];
                }
            });
        }
    }

    fetchData() {
        for (var key in this.form) {
            if (this.form.hasOwnProperty(key)) {
                if ($('input#' + key + '').length > 0) {
                    this.form[key] = $('input#' + key + '').val();
                } else {
                    this.form[key] = $('select#' + key + '').val();
                }
            }
        }
    }

    save() {
        this.form.pic = $('.signature-pic').attr('src');

        $.post('http://backoffice.springair.com.pt/signatures', this.form, function (data) {
            $('.signature-pic').attr('src', data);
        });
    }
};