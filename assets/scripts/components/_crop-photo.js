class cropPhoto {
    constructor() {
        this.crop = $('#uploaded-photo').croppie({
            enableExif: true,
            viewport: {
                width: 125,
                height: 125,
                type: 'square'
            }
        });
    }

    readFile(input) {
        if (input.files && input.files[0]) {
            const reader = new FileReader();

            reader.onload = (e) => {
                $('#upload-photo--container').addClass('ready');
                this.crop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('Bind complete');
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    cropPhoto() {
        this.crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (result) {
            $('.signature-pic').attr('src', result);
        });
    }
}