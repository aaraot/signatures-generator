$(document).ready(function () {
    let signature = new signatureGenerator();
    let crop = new cropPhoto();

    $('#pic').on('change', function () {
        crop.readFile(this);
    });

    $('.upload-photo').on('click', function () {
        $(this).prev().click();
    });

    $('.crop-photo').on('click', function () {
        crop.cropPhoto();
    });

    $('#generate').click(function () {
        if ($('.invalid').length === 0) {
            signature.generate();
        }
    });

    $('#clipboard').click(function () {
        signature.save();
    });

    new ClipboardJS('#clipboard');
});